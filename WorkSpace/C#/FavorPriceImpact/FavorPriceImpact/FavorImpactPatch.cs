﻿using System;
using System.Collections.Generic;
using System.Text;
using HarmonyLib;
using RimWorld;
using Verse;

namespace FavorPriceImpact
{
  [HarmonyPatch(typeof(StatExtension), "GetStatValue")]
  public class FavorImpactPatch
    {
    [HarmonyPostfix]
    private static void InjectHediffRoyalFavorValue(Thing thing, StatDef stat, ref float __result)
		{
			//rewrite to :
			//Pawn get Race 
			//Get Royal Value of race 
			//Get Hediff list on Pawn
			//Look for spawnThingOnRemoved (what we want is in their statBase)
			//If spawnThingOnRemoved ThingDef has FavorValue add said Value to Pawn stat
			try
			{
				if(!(thing is Pawn))
				{
					return;
				}
				if(stat != StatDefOf.RoyalFavorValue)
				{
					return;
				}
				Pawn pawn = (Pawn)thing;
				List<Hediff> hediffs = pawn.health?.hediffSet?.hediffs;
				if (hediffs.NullOrEmpty())
				{
					return;
				}
				foreach (Hediff hediff in hediffs)//logic after here need to be culled badly
				{
					__result += GetFavor(hediff.def);
				}
			}
      catch (Exception e)
      {
        Log.Warning("FavorPriceImpact: Something went wrong " + e);
        return;
      }
    }
		private static float GetFavor(HediffDef hediffDef)
		{
			ThingDef implantThing = hediffDef.spawnThingOnRemoved;
			if(implantThing != null)
			{
				float favorOffset = implantThing.GetStatValueAbstract(StatDefOf.RoyalFavorValue);
				//Log.Message(implantThing.label + " has favor offset value: " + favorOffset);
				return favorOffset;
			}
			return 0;
		}
  }
}
