﻿using HarmonyLib;
using System.Reflection;
using Verse;

namespace FavorPriceImpact
{
	[StaticConstructorOnStartup]
	public static class Startup
	{
		static Startup()
		{
			Harmony harmony = new Harmony("Favor Price Impact");
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
	}
}
